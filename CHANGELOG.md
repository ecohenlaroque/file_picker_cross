## 1.2.0

* Fixed crashes on io-Devices (Desktop, Android, iOS) if the fileExtension is empty

## 1.1.0

* Fixed compatibility issues for Android and iOS

## 1.0.0

* Basic functionality working on the web
